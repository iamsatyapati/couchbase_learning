CouchBase Introduction
----------------------
It's a document based, open source nosql database with following features:
1. Distributed
2. Fault tolerence: persistent, replicated
3. schema free and flexible
4. High performance and easy to scale. 

Components of CouchBase
-----------------------
1. Cluster: A set of nodes.
2. Node: On which couchbase services are running.
3. Vbuckets: Vbuckets contains buckets.
4. Buckets: Actual document stored here.

Vbuckets
--------
- The overall key partitioned into 1024 logical storage unit. 
- It's distributed across machine within the cluster via a map that is shared among the servers in the clusters as well as in the client library.
- Couchbase uses Ketama algorithm to get a hash value of the document.
- Each key is hashed to a vbuckets.

A cluster contains a number of nodes. Each node contains a number of buckets. Each buckets contain vbuckets. Each vbuckets contain the unique key which is mapped to a json document.

